package br.com.letscode;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Developer {

    private String nome;
    private String sobrenome;
    private String tecnologiaPreferida;
    private Integer anosExperiencia;
}
