package br.com.letscode;

import java.util.Arrays;
import java.util.stream.Collectors;

public class DeveloperFactory {

    public static final int INDICE_COLUNA_NOME = 0;
    public static final int INDICE_COLUNA_SOBRENOME = 1;
    public static final int INDICE_COLUNA_TECNOLOGIA = 2;
    public static final int INDICE_COLUNA_EXPERIENCIA = 3;
    private final String[] values;

    public DeveloperFactory(String[] values) {
        this.values = values;
    }

    public Developer create() {
        var valuesList = Arrays.stream(values)
                .map(String::trim)
                .collect(Collectors.toList());

        return Developer.builder()
                .nome(valuesList.get(INDICE_COLUNA_NOME))
                .sobrenome(valuesList.get(INDICE_COLUNA_SOBRENOME))
                .tecnologiaPreferida(valuesList.get(INDICE_COLUNA_TECNOLOGIA))
                .anosExperiencia(Integer.valueOf(valuesList.get(INDICE_COLUNA_EXPERIENCIA)))
                .build();
    }
}
