package br.com.letscode;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Application {

    public static void main(String[] args) throws IOException {
        String filePath = "docs".concat(File.separator).concat("devs.csv");
        File devsFile = FileUtils.getFile(filePath);

        List<String> developersList = FileUtils.readLines(devsFile, Charset.defaultCharset())
                .stream()
                .map(line -> line.split(","))
                .map(values -> {
                    var factory = new DeveloperFactory(values);
                    return factory.create();
                })
                .map(Developer::toString)
                .peek(log::info)
                .collect(Collectors.toList());

        String writeFilePath = "docs".concat(File.separator).concat("devs-java.txt");
        File writeFile = FileUtils.getFile(writeFilePath);
        boolean append = true;
        FileUtils.writeLines(writeFile, developersList, append);
    }
}
